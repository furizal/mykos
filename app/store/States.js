Ext.define('myKos.store.States', {
    extend: 'Ext.data.Store',
    alias: 'store.states',

    fields: ['countryId', 'name'],

    data: [{
        countryId: 1,
        name: 'Simpang Tiga'
    }, {
        countryId: 1,
        name: 'Tangkerang Labuai'
    }, {
        countryId: 1,
        name: 'Tangkerang Selatan'
    },{
        countryId: 1,
        name: 'Tangkerang Utara'
    }, {
        countryId: 1,
        name: 'Air Dingin'
    }, {
        countryId: 2,
        name: 'Bambu Kuning'
    },{
        countryId: 2,
        name: 'Bencah Lesung'
    }, {
        countryId: 2,
        name: 'Industri Tenayan'
    }, {
        countryId: 2,
        name: 'Kulim'
    },{
        countryId: 2,
        name: 'Melebung'
    }, {
        countryId: 2,
        name: 'Mentangor'
    }, {
        countryId: 2,
        name: 'Pebatuan'
    },{
        countryId: 2,
        name: 'Pematang Kapau'
    }, {
        countryId: 2,
        name: 'Rejosari'
    }, {
        countryId: 2,
        name: 'Sialang Rampai'
    },{
        countryId: 2,
        name: 'Sialang Sakti'
    }, {
        countryId: 2,
        name: 'Tangkerang Timur'
    }, {
        countryId: 2,
        name: 'Tuah Negeri'
    },{
        countryId: 3,
        name: 'Agrowisata'
    }, {
        countryId: 3,
        name: 'Maharani'
    }, {
        countryId: 3,
        name: 'Muara Fajar Barat'
    },{
        countryId: 3,
        name: 'Muara Fajar Timur'
    }, {
        countryId: 3,
        name: 'Palas'
    }, {
        countryId: 3,
        name: 'Rantau Panjang'
    },{
        countryId: 3,
        name: 'Rumbai Bukit'
    }, {
        countryId: 3,
        name: 'Sri Meranti'
    }, {
        countryId: 3,
        name: 'Umban Sari'
    }, {
        countryId: 4,
        name: 'Lembah Damai'
    }, {
        countryId: 4,
        name: 'Lembah Sari'
    },{
        countryId: 4,
        name: 'Limbungan'
    }, {
        countryId: 4,
        name: 'Limbungan Baru'
    }, {
        countryId: 4,
        name: 'Meranti Pandak'
    },{
        countryId: 4,
        name: 'Sungai Ambang'
    }, {
        countryId: 4,
        name: 'Sungai Ukai'
    }, {
        countryId: 4,
        name: 'Tebing Tinggi Okura'
    }, {
        countryId: 5,
        name: 'Air Hitam'
    }, {
        countryId: 5,
        name: 'Bandar Raya'
    },{
        countryId: 5,
        name: 'Labuh Baru Barat'
    }, {
        countryId: 5,
        name: 'Labuh Baru Timur'
    }, {
        countryId: 5,
        name: 'Sungai Sibam'
    },{
        countryId: 5,
        name: 'Tampan'
    }, {
        countryId: 5,
        name: 'Tirta Siak'
    }, {
        countryId: 6,
        name: 'Maharatu'
    }, {
        countryId: 6,
        name: 'Perhentian Marpoyan'
    },{
        countryId: 6,
        name: 'Sidomulyo Timur'
    }, {
        countryId: 6,
        name: 'Tangkerang Barat'
    }, {
        countryId: 6,
        name: 'Tangkerang Tengah'
    },{
        countryId: 6,
        name: 'Wonorejo'
    }, {
        countryId: 7,
        name: 'Harjosari'
    }, {
        countryId: 7,
        name: 'Jadirejo'
    },{
        countryId: 7,
        name: 'Kampung Melayu'
    }, {
        countryId: 7,
        name: 'Kampung Tengah'
    }, {
        countryId: 7,
        name: 'Kedung Sari'
    },{
        countryId: 7,
        name: 'Pulau Karam'
    }, {
        countryId: 7,
        name: 'Sukajadi'
    }, {
        countryId: 8,
        name: 'Kampung Bandar'
    }, {
        countryId: 8,
        name: 'Kampung Baru'
    },{
        countryId: 8,
        name: 'Kampung Dalam'
    }, {
        countryId: 8,
        name: 'Padang Bulan'
    }, {
        countryId: 8,
        name: 'Padang Terubuk'
    },{
        countryId: 8,
        name: 'Sago'
    },{
        countryId: 9,
        name: 'Cinta Raja'
    }, {
        countryId: 9,
        name: 'Sukamaju'
    },{
        countryId: 9,
        name: 'Sukamulya'
    }, {
        countryId: 10,
        name: 'Simpang Empat'
    }, {
        countryId: 10,
        name: 'Sumahilang'
    },{
        countryId: 10,
        name: 'Tanah Datar'
    }, {
        countryId: 10,
        name: 'Kota Baru'
    }, {
        countryId: 10,
        name: 'Sukaramai'
    },{
        countryId: 10,
        name: 'Kota Tinggi'
    }, {
        countryId: 11,
        name: 'Pesisir'
    }, {
        countryId: 11,
        name: 'Rintis'
    },{
        countryId: 11,
        name: 'Tanjung Rhu'
    }, {
        countryId: 11,
        name: 'Sekip'
    }, {
        countryId: 12,
        name: 'Air Putih'
    }, {
        countryId: 12,
        name: 'Bina Widya'
    },{
        countryId: 12,
        name: 'Delima'
    }, {
        countryId: 12,
        name: 'Sialang Munggu'
    }, {
        countryId: 12,
        name: 'Sidomulyo Barat'
    },{
        countryId: 12,
        name: 'Simpang Baru'
    }, {
        countryId: 12,
        name: 'Tobek Godang'
    }, {
        countryId: 12,
        name: 'Tuah Karya'
    }, {
        countryId: 12,
        name: 'Tuah Madani'
    }]
});