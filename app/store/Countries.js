Ext.define('myKos.store.Countries', {
    extend: 'Ext.data.Store',
    alias: 'store.countries',

    fields: ['id', 'name'],

    data: [{
                id: 1,
                name: 'Bukit Raya'
            }, {
                id: 2,
                name: 'Tenayan Raya'
            },
            {
                id: 3,
                name: 'Rumbai'
            }, 
            {
                id: 4,
                name: 'Rumbai Pesisir'
            },
            {
                id: 5,
                name: 'Payung Sakaki'
            }, 
            {
                id: 6,
                name: 'Marpoyan Damai'
            },
            {
                id: 7,
                name: 'Sukajadi'
            },
            {
                id: 8,
                name: 'Senapelan'
            },
            {
                id: 9,
                name: 'Sail'
            }, 
            {
                id: 10,
                name: 'Pekanbaru Kota'
            },
            {
                id: 11,
                name: 'Lima Puluh'
            }, 
            {
                id: 12,
                name: 'Tampan'
            }
        ]
});